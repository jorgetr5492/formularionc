const API_URL= "https://web-test-technical.herokuapp.com/apinode"

export async function getAllContacts(){
    try{
    const response = await fetch(API_URL);
    const data = await response.json();
    return data;
    }catch(err){
        console.error(err);
    }
}