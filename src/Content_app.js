import datos from './datos.svg';
import facebook from './facebook.svg';
import historial from './historial.svg';
import instagram from './instagram.svg';
import reconocimiento from './reconocimiento.svg';
import Toast from 'react-bootstrap/Toast';
import { useState } from 'react';
import './Contentapp.css';

const capturarDatos = (e)=>{
  e.preventDefault();
  // console.log(document.getElementById('name'));

  fetch('https://web-test-technical.herokuapp.com/apinode', {
    method: 'POST',
    mode:'cors',
    headers: {

      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Control-Allow-Origin':'https://web-test-technical.herokuapp.com',
      'Access-Control-Allow-Methods':' POST, GET'
    },
    body: JSON.stringify({
      "name":document.getElementById('name').value,
      "email":document.getElementById('email').value,
      "phone":document.getElementById('phone').value
    }),
  });
 


  
  document.getElementById('name').value="";
  document.getElementById('email').value="";
   document.getElementById('phone').value="";
 
} 

function Content_app() {
  const [show, setShow] = useState(false);

  return (
    <div className="content_root" >
        <p className="content_title">¿Nemo enim ipsam voluptatem quia  
        <b className="content_subtitle">75,000 voluptas sit aspernatur?</b></p> 
        <div className="box">
            <div className="item-box">
                <img src={datos} className="content-logo" alt="datos" /> 
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod </p>
            </div>
            <div className="item-box">
                <img src={historial} className="content-logo2" alt="historial" /> 
                 <p> Duis aute irure dolor in reprehenderit 14,230  </p>
            </div>
            <div className="item-box">
                <img src={reconocimiento} className="content-logo" alt="reconocimiento" /> 
                 <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium <b>“doloremque laudantium, totam rem aperiam”</b></p>
            </div>
 
      </div>
      <div className="box">
            <div className="item-box">
                <img src={facebook} className="content-logo" alt="facebook" /> 
                 <p> Lorem 185,000 ipsum dolor sit amet, consectetur</p>
            </div>
            <div className="item-box">
                <img src={instagram} className="content-logo" alt="instagram" /> 
                 <p>Sed ut 30,000 perspiciatis unde omnis </p>
            </div>
 

      </div>
      <div className="login">
      <Toast onClose={() => setShow(false)} show={show} delay={2000} autohide>
          <Toast.Header>
         
            <strong className="me-auto">Company Name</strong>
            <small>now</small>
          </Toast.Header>
          <Toast.Body>Registro Exitoso</Toast.Body>
        </Toast>
        <h1><center>Déjanos tus datos</center> </h1>
        <form onSubmit={capturarDatos}>

            <label >Nombre</label>
            <input
              type="text"
              name="name"
              id="name"
              className="input_box"
              required
            />


            <label >Teléfono</label>
            <input
              type="phone"
              name="phone"
              id="phone"
              className="input_box"
              required
            />
            <label >Correo Electronico</label>
            <input
              type="Email"
              name="Email"
              id="email"
              className="input_box"
              required
            />

  
        <br />
          <button className="btn_c" onClick={() => setShow(true)} type="submit">Enviar</button>
        </form>
      </div>
     
    </div>
  );
}


export default Content_app;