import datos from './datos.svg';
import facebook from './facebook.svg';
import historial from './historial.svg';
import instagram from './instagram.svg';
import reconocimiento from './reconocimiento.svg';
import Table from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Contentapp.css';


function Content_app() {

    const listas =  fetch('https://web-test-technical.herokuapp.com/apinode', {
        method: 'GET',

        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
     
        }
       
      });
      console.log(listas);
  return (
    <div className="content_root" >
        <p className="content_title">¿Nemo enim ipsam voluptatem quia  
        <b className="content_subtitle">75,000 voluptas sit aspernatur?</b></p> 
        <div className="box">
            <div className="item-box">
                <img src={datos} className="content-logo" alt="datos" /> 
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod </p>
            </div>
            <div className="item-box">
                <img src={historial} className="content-logo2" alt="historial" /> 
                 <p> Duis aute irure dolor in reprehenderit 14,230  </p>
            </div>
            <div className="item-box">
                <img src={reconocimiento} className="content-logo" alt="reconocimiento" /> 
                 <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium <b>“doloremque laudantium, totam rem aperiam”</b></p>
            </div>
 
      </div>
      <div className="box">
            <div className="item-box">
                <img src={facebook} className="content-logo" alt="facebook" /> 
                 <p> Lorem 185,000 ipsum dolor sit amet, consectetur</p>
            </div>
            <div className="item-box">
                <img src={instagram} className="content-logo" alt="instagram" /> 
                 <p>Sed ut 30,000 perspiciatis unde omnis </p>
            </div>
 

      </div>
      <div className="login">
        <h1><center>Déjanos tus datos</center> </h1>
        <Table striped bordered hover>
  <thead>

    <tr>

      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
  {
      listas.map((listac)=>(
          <div>a</div>
      ))

  }
   
 
  </tbody>
</Table>
      </div>
     
    </div>
  );
}


export default Content_app;