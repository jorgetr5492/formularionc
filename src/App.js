import logo from './logo.png';
import './App.css';
import Content_app from './Content_app';
import Content_list from './Content_list';
import { Routes, Route, Link } from "react-router-dom";


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />   
      </header>
      <section >
        
      <Routes>
        <Route path="/" element={<Content_app />} />
        <Route path="list" element={<Content_list />} />
      </Routes>
     
        
      </section>
      <footer className="App-footer">
      <p> <b>Company Name </b><br/>
       2022</p>
      </footer>
    </div>
  );
}

export default App;
